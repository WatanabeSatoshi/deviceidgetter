package jp.logiclogic.deviceidgetter;

import java.util.ArrayList;
import java.util.HashMap;

import android.support.v7.app.ActionBarActivity;
import android.support.v4.app.Fragment;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.content.Context;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

public class MainActivity extends ActionBarActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		// 端末IDとして使えそうなIDを全部とる
		ArrayList<HashMap<String, String>> id_list = new ArrayList<HashMap<String, String>>();
		HashMap<String, String> device_hash = new HashMap<String, String>();
		String ANDROID_ID = android.provider.Settings.System.getString(
				getContentResolver(),
				android.provider.Settings.Secure.ANDROID_ID);
		device_hash.put("name", "ANDROID_ID");
		device_hash.put("value", ANDROID_ID);
		id_list.add(device_hash);

		device_hash = new HashMap<String, String>();
		TelephonyManager mTelephonyMgr = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);
		String IMSI = mTelephonyMgr.getSubscriberId();
		device_hash.put("name", "IMSI");
		device_hash.put("value", IMSI);
		id_list.add(device_hash);

		device_hash = new HashMap<String, String>();
		String ICCID = mTelephonyMgr.getSimSerialNumber();
		device_hash.put("name", "ICCID");
		device_hash.put("value", ICCID);
		id_list.add(device_hash);

		device_hash = new HashMap<String, String>();
		String IMEI = mTelephonyMgr.getDeviceId();
		device_hash.put("name", "IMEI");
		device_hash.put("value", IMEI);
		id_list.add(device_hash);

		device_hash = new HashMap<String, String>();
		WifiManager wifiManager = (WifiManager) getSystemService(Context.WIFI_SERVICE);
		WifiInfo wifiInfo = wifiManager.getConnectionInfo();
		String MACADDRESS = wifiInfo.getMacAddress();
		device_hash.put("name", "MACADDRESS");
		device_hash.put("value", MACADDRESS);
		id_list.add(device_hash);

		if (savedInstanceState == null) {
			PlaceholderFragment flagment = new PlaceholderFragment(id_list);
			getSupportFragmentManager().beginTransaction()
					.add(R.id.container, flagment).commit();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment {

		private ArrayList<HashMap<String, String>> id_list;

		public PlaceholderFragment(ArrayList<HashMap<String, String>> id_list) {
			this.id_list = id_list;
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_main, container,
					false);

			LinearLayout incLayout = null;
			LinearLayout mainLayout = (LinearLayout)rootView.findViewById(R.id.display);

			for(int i = 0; i < id_list.size() ; i++){
				HashMap<String, String> line = id_list.get(i);
				incLayout = (LinearLayout)inflater.inflate(R.layout.flame_deviceinfo, null);
				TextView name = (TextView)incLayout.findViewById(R.id.name);
				TextView value = (TextView)incLayout.findViewById(R.id.value);
				name.setText(line.get("name"));
				Log.d("DeviceIdGetter", line.get("name"));
				value.setText(line.get("value"));
				mainLayout.addView(incLayout);
			}
			return rootView;
		}
	}

}
