//
//  MainViewController.m
//  DeviceIdGetter
//
//  Created by 渡辺 聡志 on 2014/04/25.
//  Copyright (c) 2014年 渡辺 聡志. All rights reserved.
//

#import <AdSupport/ASIdentifierManager.h>
#import "MainViewController.h"

#import "NetworkInformation.h"
#import "SecureUDID.h"
#import "OpenUDID.h"
#import "OpenIDFA.h"

#import <stdio.h>
#import <string.h>
#import <sys/types.h>
#import <sys/ioctl.h>
#import <netinet/in.h>
#import <net/if.h>
#import <GameKit/GameKit.h>

@interface MainViewController ()

@end

@implementation MainViewController

- (void)loadView
{
    [super loadView];
    
    // BaseLabel
    UITextView *text = [[UITextView alloc] initWithFrame:CGRectMake(10, 20, self.view.frame.size.width-10, self.view.frame.size.height-20)];
    [text setFont:[UIFont boldSystemFontOfSize:11]];
    
    // UUID
    /*
     // 以前の取り方
    CFUUIDRef uuidObj = CFUUIDCreate(nil);//create a new UUID
    //get the string representation of the UUID
    NSString *uuidString = (NSString*)CFUUIDCreateString(nil, uuidObj);
    CFRelease(uuidObj);
    NSString *uuid [uuidString autorelease];
     */
    NSString *uuid = [[NSUUID UUID] UUIDString];
    if(uuid == nil) uuid = @"null";
    // UIDevice.identifierForVendor
    NSString *identifierForVendor;
    if (floor(NSFoundationVersionNumber) < NSFoundationVersionNumber_iOS_6_0) {
        identifierForVendor = @"取得不可";
    }else{
        identifierForVendor = [UIDevice currentDevice].identifierForVendor.UUIDString;
    }
    if(identifierForVendor == nil) identifierForVendor = @"null";
    // プライマリMacアドレス(wifi)
    NSString *pmacaddr = [[[NetworkInformation alloc] init] primaryMACAddress];
    if(pmacaddr == nil) pmacaddr = @"null";
    
    int fd;
    struct ifreq ifr;
    fd = socket(AF_INET, SOCK_DGRAM, 0);
    ifr.ifr_addr.sa_family = AF_INET;
    strncpy(ifr.ifr_name, "eth0", IFNAMSIZ-1);
    ioctl(fd, SIOCGIFADDR, &ifr);
    pmacaddr = [NSString stringWithFormat:@"%.2x:%.2x:%.2x:%.2x:%.2x:%.2x:",
                (unsigned char)ifr.ifr_ifru.ifru_addr.sa_data[0],
                (unsigned char)ifr.ifr_ifru.ifru_addr.sa_data[1],
                (unsigned char)ifr.ifr_ifru.ifru_addr.sa_data[2],
                (unsigned char)ifr.ifr_ifru.ifru_addr.sa_data[3],
                (unsigned char)ifr.ifr_ifru.ifru_addr.sa_data[4],
                (unsigned char)ifr.ifr_ifru.ifru_addr.sa_data[5]];
    GKLocalPlayer *pl = [GKLocalPlayer localPlayer];
    pmacaddr = pl.playerID;
    
    // セカンダリMacアドレス(3G)
    NSString *smacaddr = [[[NetworkInformation alloc] init] MACAddressForInterfaceName:@"en1"];
    if(smacaddr == nil) smacaddr = @"null";
    // SecureUDID
    NSString *sudid = [SecureUDID UDIDForDomain:[[NSBundle mainBundle] bundleIdentifier] usingKey:@"dso8uq3rawiya0rafaa3kwtgtag3as73"];
    if(sudid == nil) sudid = @"null";
    // IDFA
    // これを使う場合はadvertisingIdentifierEnabledをチェックしてユーザが許可していることを確認しないといけない
    NSString *idfa = [[ASIdentifierManager sharedManager].advertisingIdentifier UUIDString];
    if(idfa == nil) idfa = @"null";
    // OpenUDID
    NSString *openUDID = [OpenUDID value];
    if(openUDID == nil) openUDID = @"null";
    // OpenIDFA
    NSString *openIDFA = [OpenIDFA sameDayOpenIDFA];
    if(openIDFA == nil) openIDFA = @"null";
    
    if (floor(NSFoundationVersionNumber) < NSFoundationVersionNumber_iOS_6_0) {
        NSString* w_text = @"UDID : "
        @"最新のiOS7対応のxcodeではメソッドが削除され、取得不可能\n"
        @"\n"
        @"UUID : ";
        w_text = [w_text stringByAppendingString:uuid];
        w_text = [w_text stringByAppendingString:@"\n   公式実装。以前の取り方がiOS7ではできなく、公式で用意されているNSUUIDクラスより生成する必要あり\n"
        @"\n"
         @"primaryMacAddr(wifi) : "];
        w_text = [w_text stringByAppendingString:pmacaddr];
        w_text = [w_text stringByAppendingString:@"\n   非公式実装。iOS7より正常に取得できない\n"
        @"\n"
         @"secondaryMacAddr(3G) : "];
        w_text = [w_text stringByAppendingString:smacaddr];
        w_text = [w_text stringByAppendingString:@"\n   非公式実装。iOS7より正常に取得できない\n"
        @"\n"
         @"SecureUDID : "];
        w_text = [w_text stringByAppendingString:sudid];
        w_text = [w_text stringByAppendingString:@"\n   非公式実装。開発元よりiOS6以降は公式メソッドを使って下さいとなっており。非推奨となっている。\n"
        @"\n"
         @"UIDevice.identifierForVendor : "];
        w_text = [w_text stringByAppendingString:identifierForVendor];
        w_text = [w_text stringByAppendingString:@"\n   公式実装。iOS6より使用可能なUDIDの代替。\n"
        @"\n"
         @"IDFA : "];
        w_text = [w_text stringByAppendingString:idfa];
        w_text = [w_text stringByAppendingString:@"\n   公式実装。iOS6より使用可能。\n"
        @"\n"
         @"OpenUDID : "];
        w_text = [w_text stringByAppendingString:openUDID];
        w_text = [w_text stringByAppendingString:@"\n   非公式実装。\n"
        @"\n"
         @"OpenIDFA : "];
        w_text = [w_text stringByAppendingString:openIDFA];
        w_text = [w_text stringByAppendingString:@"\n   非公式実装。生成するIDは有効期間があり、1日もしくは３日間有効なものしか発行できない。\n"
         @"\n"];

        [text setText:w_text];
    }
    else{
        NSDictionary *blackfont = @{ NSForegroundColorAttributeName : [UIColor blackColor],
                               NSFontAttributeName : [UIFont boldSystemFontOfSize:11.0f] };
        NSDictionary *redfont = @{ NSForegroundColorAttributeName : [UIColor redColor],
                                         NSFontAttributeName : [UIFont boldSystemFontOfSize:11.0f] };
        NSMutableAttributedString *mutableAttributedString = [[NSMutableAttributedString alloc] init];
    
        [mutableAttributedString appendAttributedString:[
                                                     [NSAttributedString alloc]
                                                     initWithString:@"UDID : " attributes:blackfont]];
    
        [mutableAttributedString appendAttributedString:[
                                                     [NSAttributedString alloc]
                                                     initWithString:@"最新のiOS7対応のxcodeではメソッドが削除され、取得不可能\n" attributes:redfont]];
        [mutableAttributedString appendAttributedString:[
                                                     [NSAttributedString alloc]
                                                     initWithString:@"\n" attributes:blackfont]];
    
        [mutableAttributedString appendAttributedString:[
                                                     [NSAttributedString alloc]
                                                     initWithString:@"UUID : " attributes:blackfont]];
        [mutableAttributedString appendAttributedString:[
                                                     [NSAttributedString alloc]
                                                     initWithString:uuid attributes:redfont]];
        [mutableAttributedString appendAttributedString:[
                                                     [NSAttributedString alloc]
                                                     initWithString:@"\n   公式実装。以前の取り方がiOS7ではできなく、公式で用意されているNSUUIDクラスより生成する必要あり\n" attributes:blackfont]];
        [mutableAttributedString appendAttributedString:[
                                                     [NSAttributedString alloc]
                                                     initWithString:@"\n" attributes:blackfont]];
    
        [mutableAttributedString appendAttributedString:[
                                                     [NSAttributedString alloc]
                                                     initWithString:@"primaryMacAddr(wifi) : " attributes:blackfont]];
        [mutableAttributedString appendAttributedString:[
                                                     [NSAttributedString alloc]
                                                     initWithString:pmacaddr attributes:redfont]];
        [mutableAttributedString appendAttributedString:[
                                                     [NSAttributedString alloc]
                                                     initWithString:@"\n   非公式実装。iOS7より正常に取得できない\n" attributes:blackfont]];
        [mutableAttributedString appendAttributedString:[
                                                     [NSAttributedString alloc]
                                                     initWithString:@"\n" attributes:blackfont]];
    
    
        [mutableAttributedString appendAttributedString:[
                                                     [NSAttributedString alloc]
                                                     initWithString:@"secondaryMacAddr(3G) : " attributes:blackfont]];
        [mutableAttributedString appendAttributedString:[
                                                     [NSAttributedString alloc]
                                                     initWithString:smacaddr attributes:redfont]];
        [mutableAttributedString appendAttributedString:[
                                                     [NSAttributedString alloc]
                                                     initWithString:@"\n   非公式実装。iOS7より正常に取得できない\n" attributes:blackfont]];
        [mutableAttributedString appendAttributedString:[
                                                     [NSAttributedString alloc]
                                                     initWithString:@"\n" attributes:blackfont]];
    
        [mutableAttributedString appendAttributedString:[
                                                     [NSAttributedString alloc]
                                                     initWithString:@"SecureUDID : " attributes:blackfont]];
        [mutableAttributedString appendAttributedString:[
                                                     [NSAttributedString alloc]
                                                     initWithString:sudid attributes:redfont]];
        [mutableAttributedString appendAttributedString:[
                                                     [NSAttributedString alloc]
                                                     initWithString:@"\n   非公式実装。開発元よりiOS6以降は公式メソッドを使って下さいとなっており。非推奨となっている。\n" attributes:blackfont]];
        [mutableAttributedString appendAttributedString:[
                                                     [NSAttributedString alloc]
                                                     initWithString:@"\n" attributes:blackfont]];
    
        [mutableAttributedString appendAttributedString:[
                                                     [NSAttributedString alloc]
                                                     initWithString:@"UIDevice.identifierForVendor : " attributes:blackfont]];
        [mutableAttributedString appendAttributedString:[
                                                     [NSAttributedString alloc]
                                                     initWithString:identifierForVendor attributes:redfont]];
        [mutableAttributedString appendAttributedString:[
                                                     [NSAttributedString alloc]
                                                     initWithString:@"\n   公式実装。iOS6より使用可能なUDIDの代替。\n" attributes:blackfont]];
        [mutableAttributedString appendAttributedString:[
                                                     [NSAttributedString alloc]
                                                     initWithString:@"\n" attributes:blackfont]];
    
        [mutableAttributedString appendAttributedString:[
                                                     [NSAttributedString alloc]
                                                     initWithString:@"IDFA : " attributes:blackfont]];
        [mutableAttributedString appendAttributedString:[
                                                     [NSAttributedString alloc]
                                                     initWithString:idfa attributes:redfont]];
        [mutableAttributedString appendAttributedString:[
                                                     [NSAttributedString alloc]
                                                     initWithString:@"\n   公式実装。iOS6より使用可能。\n" attributes:blackfont]];
        [mutableAttributedString appendAttributedString:[
                                                     [NSAttributedString alloc]
                                                     initWithString:@"\n" attributes:blackfont]];
    
        [mutableAttributedString appendAttributedString:[
                                                     [NSAttributedString alloc]
                                                     initWithString:@"OpenUDID : " attributes:blackfont]];
        [mutableAttributedString appendAttributedString:[
                                                     [NSAttributedString alloc]
                                                     initWithString:openUDID attributes:redfont]];
        [mutableAttributedString appendAttributedString:[
                                                     [NSAttributedString alloc]
                                                     initWithString:@"\n   非公式実装。\n" attributes:blackfont]];
        [mutableAttributedString appendAttributedString:[
                                                     [NSAttributedString alloc]
                                                     initWithString:@"\n" attributes:blackfont]];
    
        [mutableAttributedString appendAttributedString:[
                                                     [NSAttributedString alloc]
                                                     initWithString:@"OpenIDFA : " attributes:blackfont]];
        [mutableAttributedString appendAttributedString:[
                                                     [NSAttributedString alloc]
                                                     initWithString:openIDFA attributes:redfont]];
        [mutableAttributedString appendAttributedString:[
                                                     [NSAttributedString alloc]
                                                     initWithString:@"\n   非公式実装。生成するIDは有効期間があり、1日もしくは３日間有効なものしか発行できない。\n" attributes:blackfont]];
        [mutableAttributedString appendAttributedString:[
                                                     [NSAttributedString alloc]
                                                     initWithString:@"\n" attributes:blackfont]];
        [text setAttributedText:mutableAttributedString];
    }
    text.editable = NO;
    [self.view addSubview:text];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
